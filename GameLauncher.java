import java.util.Scanner;
public class GameLauncher{
	
	//This main method ask the user if they want to play a game or not.
	//If the user answered yes, they have to choose between two games.
	//They can also play again if they want.
	public static void main(String[] args){
		Scanner reader = new Scanner(System.in);
		
		System.out.println("Hi, do you want to play Hangman or Wordle, yes or no?");
		String answer = reader.next();
		
		while(answer.equals("yes")){
			System.out.println("Enter 1 to play Hangman or 2 to play Wordle.");
			int gameChosen = reader.nextInt();
			if(gameChosen == 1){
				hangman();
				System.out.println("Great! Do you want to play again, yes or no?");
				answer = reader.next();
			}
			else if(gameChosen == 2){
				wordle();
				System.out.println("Great! Do you want to play again, yes or no?");
				answer = reader.next();
			}
			else{
				System.out.println("Choose a game!");
			}
		}
	}
	
	//This method asks the user to enter a 4 letter word and then call the runGame method.
	public static void hangman(){
		Scanner read = new Scanner(System.in);
		System.out.println("Please enter a 4 letter word with 4 different letter");
		String word = read.next();
		WordGuesser.runGame(word);
	}
	
	//This method just choose a word from the array list in the method generateWord and then store it into a string.
	//The string then passes through the runGame method
	public static void wordle(){
		final String word = Wordle.generateWord(); //It is final because this string stays the same throughout the whole program.
		Wordle.runGame(word);
	}
}
