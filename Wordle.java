import java.util.Random;
import java.util.Scanner;

public class Wordle{
	
	//This method contains an array that stores the words that the user will have to guess when they play the game.
	//It uses the Random class to chose the word from the array list then return it.
	public static String generateWord(){
		Random rando = new Random();
		String[] wordsChoice = {"BRASH", "NOISE", "CLAIM", "OCEAN", "GRAIN", "FIELD", "LEAST", "BREAD", "CLEAR", "BOAST", "PRIDE", "SCORN", "CRUDE", "VOICE", "DRAWN", "DOUBT", "VITAL", "LIGHT", "PLANE", "SCENT"};
		final int x = rando.nextInt(wordsChoice.length) + 1; //It is final because it will never change during the program.
		
		return wordsChoice[x];
	}
	
	//This method just verify if the letter from the user's word is inside to the word from the generateWord method.
	//If it is, then it returns true, if not, then false.
	public static boolean letterInWord(String word, char letter){
		for(int i = 0; i < word.length(); i++){
			if(letter == word.charAt(i)){
				return true;
			}
		}
		
		return false;
	}
	
	//This method do the same thing as the previous method, but also check if the position is right.
	//If it is, then it returns true, if not, then false.
	public static boolean letterInSlot(String word, char letter, int position){
		for(int i = 0; i < word.length(); i++){
			if(letter == word.charAt(i) && position == i){
				return true;
			}
		}
		
		return false;
	}
	
	//This method contains an array that sets the initial colour to white for each letter.
	//It sets the colour of the letter to green if the letterInSlot method returned true.
	//It sets the colour of the letter to yellow if the letterInWord method returned true.
	//If either of them return false, it doesn't do anything. The colour stay white for that letter.
	//At the end, this method returns the array colours.
	public static String[] guessWord(String answer, String guess){
		String[] letterColours = {"white", "white", "white", "white", "white"};

		for(int i = 0; i < guess.length(); i++){
			char letter = guess.charAt(i);
			if(letterInSlot(answer, letter, i)){
				letterColours[i] = "green";
			}else if(letterInWord(answer, letter)){
				letterColours[i] = "yellow";
			}
		}
		
		return letterColours;
	}
	
	//This method takes a word and an array colours.
	//It just print out the colour of each letter in the word depending from the result of the array colours.
	//It doesn't return anything.
	public static void presentResults(String word, String[] colours){
		final String GREEN = "\u001B[32m";
		final String YELLOW = "\u001B[33m";
		final String WHITE = "\u001B[37m";
		
		for(int i = 0; i < word.length(); i++){
			char letterPosition = word.charAt(i);
			if(colours[i].equals("green")){
				System.out.print(GREEN + letterPosition + WHITE); //It goes back to white after, so it doesn't mess up the messages that follows it.
			}
			else if(colours[i].equals("yellow")){
				System.out.print(YELLOW + letterPosition + WHITE); //Same for this one.
			}
			else{
				System.out.print(WHITE + letterPosition);
			}
		}
	}
	
	//This method ask the user to input a five letter word as a guess.
	//It goes on until the word entered has the length of five.
	//It then returns the words in capitals.
	public static String readGuess(){
		Scanner reader = new Scanner(System.in);
		
		System.out.println("Please enter a 5 letter word:");
		String userGuess = reader.next();
		
		while(userGuess.length() != 5){
			System.out.println("Please enter a 5 letter word:");
			userGuess = reader.next();
		}
		
		return userGuess.toUpperCase();
	}
	
	//This method uses the readGuess method to ask the user for a guess and stores it into a string.
	//It then uses the guessWord method to get the colours of the guess and stores it into an array.
	//It then prints the colours using the presentResults method.
	//It continues to go on until the word is found or until the user has guessed wrong six times.
	//It print some feedbacks too.
	public static void runGame(String word){
		int tries = 0;
		boolean correct = false;
		
		while(tries < 6 && correct == false){
			String guess = readGuess();
			String[] colours = guessWord(word, guess);
			
			presentResults(guess, colours);
			System.out.println();
			
			if(guess.equals(word)){
				System.out.println("You got the answer!");
				correct = true;
			}
			else{
				System.out.println("Try again.");
				tries++;
			}
		}
		if(correct == false){
			System.out.println("You didn't got the answer which was " + word);
		}
	}
}