public class WordGuesser{
	
	//This method takes a string representing a word and a char representing a letter.
	//It uses a for loop to check if the letter is in the word.
	//If it is, then it return the position in which the letter is in.
	//If it's not, then it returns -1.
	public static int isLetterInWord(String word, char c){
	
		for(int i = 0; i < word.length(); i++){
			char words = word.charAt(i);
			if(c == words){
				return i;
			}
		}
		
		return -1;
		
	}
	
	//This method takes a 4 letter word and an array of boolean that represent if the letter is correct or not. 
	//If it's a correct letter then it prints the letter, but if it's not then it just print an underscore.
	//The letter or underscore is also added to a string that prints the result.
	public static void printWord(String word, boolean[] letters){
		
		String result = "Your result is ";
		
		for(int i=0; i<4; i++){
			if(letters[i]){
				result += word.charAt(i);
			}else
				result += "_";
		}
		
		System.out.print(result);
		
	}
	
	//This method takes a word and an array of booleans initiating the 4 letters as "false".
	//It then uses a loop to ask the user to enter a letter.
	//In the loop it calls the isLetterInWord method, and then sets the boolean value of the position 
	//corresponding to the letter to true if the letter is in the word.
	//It prints the result each time after a letter is entered.
	//The loop stops when the user have guessed all the letters or when 6 mistakes have been made.
	public static void runGame(String word){
		boolean[] letters = {false, false, false, false};
		
		int misses = 0;
		
		while(misses < 6 && !(letters[0] && letters[1] && letters[2] && letters[3])){
			java.util.Scanner read = new java.util.Scanner(System.in);
			System.out.println("Please enter a letter");
			final char c = read.next().charAt(0);
			
			if(isLetterInWord(word, c) == 0){
				letters[0] = true;
			}
			else if(isLetterInWord(word, c) == 1){
				letters[1] = true;
			}
			else if(isLetterInWord(word, c) == 2){
				letters[2] = true;
			}
			else if(isLetterInWord(word, c) == 3){
				letters[3] = true;
			}
			else{
				misses++;
			}
			
			printWord(word, letters);
			System.out.println();
		}
		
	}
	
}